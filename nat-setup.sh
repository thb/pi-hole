#!/bin/bash
pihole_interface_default="eth1"
pihole_gateway_default="192.168.0.1"
external_interface_default="wlp2s0"
unset pihole_interface
unset pihole_gateway
unset external_interface

function iptables_rules() {
  echo 1 > /proc/sys/net/ipv4/ip_forward
  iptables -t nat -A POSTROUTING -o $external_interface -j MASQUERADE
  iptables -A FORWARD -i $pihole_interface -o $external_interface -j ACCEPT
  iptables -A FORWARD -i $external_interface -o $pihole_interface -m state --state RELATED,ESTABLISHED -j ACCEPT
}

function network_setup() {
  ip link set $pihole_interface up
  ip address replace $pihole_gateway/24 dev $pihole_interface
}

echo -n "pihole interface [$pihole_interface_default]: "
read pihole_interface
echo -n "pihole gateway address [$pihole_gateway_default]: "
read pihole_gateway
echo -n "external interface [$external_interface_default]: "
read ext_interface

pihole_interface=${pihole_interface:-$pihole_interface_default}
pihole_gateway=${pihole_gateway:-$pihole_gateway_default}
external_interface=${external_interface:-$external_interface_default}

network_setup
iptables_rules
