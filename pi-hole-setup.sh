#!/bin/bash
pihole_address_default="192.168.0.254/24"
gateway_default="192.168.0.1"
unset pihole_address
unset gateway

echo -n "pihole static address [$pihole_address_default]: "
read pihole_address
echo -n "gateway address [$gateway_default]: "
read gateway

pihole_address=${pihole_address:-$pihole_address_default}
gateway=${gateway:-$gateway_default}

echo "iface eth0
    static ip_address=$pihole_address
    static routers=$gateway
    static domain_name_servers=127.0.0.1" >> /etc/dhcpcd.conf
